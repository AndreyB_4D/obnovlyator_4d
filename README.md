# Обновлятор
Приложение для обновления баз данных 1С:Предприятия. 

Реализовано на OneScript (http://oscript.io). Для работы необходим OneScript версии 1.0.20 или выше.

## Описание
Обновляет базу данных из хранилища.
Обновляет позволяет максимально автоматизировать процесс обновления баз данных 1С:Предприятия. 

Имеется GUI-интерфейс для выбора базы для обновления, а также окно с выводом результата.

### Зависимости 

Зависит от:
* Библиотеки **json**: https://github.com/oscript-library/json
* Библиотеки **gui**: https://github.com/oscript-library/oscript-simple-gui
* Библиотеки **TRun1C**: https://github.com/Tavalik/TRun1C
* Библиотеки **TMail**: https://github.com/Tavalik/TMail
* Библиотеки **TLog**: https://github.com/Tavalik/TLog

## Описание и работа с приложением

Запуск приложения осуществляется запуском файла **Obnovlyator_Run.bat**.
При первом запуске в текущем каталоге будет создан пустой файл настроек **Perezalivator_Params.json**. 

Необходимо заполнить все параметры, описав возможные базы для обновления и параметры для отправки электронных писем.

Проверить корректность введенных настроек можно запустив файл **Obnovlyator_Run_Test.bat**. Обновлятор будет запущен в режиме тестирования настроек.

При следующем запуске файла **Obnovlyator_Run.bat** откроется окно, в котором необходимо выбрать базу для обновления

После указания всех исходных параметров, обновлятор начнет работу по следующему алгоритму:

1. Установка блокировки регламентных заданий и начала сеансов в базе для обновления
2. Завершение активных сеансов (спустя несколько минут) в базе
3. Подключение базы для обновления к хранилищу
4. Обновление основной конфигурации из хранилища
5. Подключение базы для обновления к хранилищу расширения
6. Обновление конфигурации расширения из хранилища
8. Обновление базы данных
7. Снятие блокировки регламентных заданий и начала сеансов базы для обновления
8. Уведомление о результате по электронной почте

Если в базе для обновления присутствуют активные соединения, будет показана таблица со всеми соединениями, а также будет предоставлена возможность завершить все активные сеансы.
Отработав, Обновлятор выдаст соответствующее сообщение (или сообщение об ошибке), а также отправит сообщение о результате работы на электронную почту.
